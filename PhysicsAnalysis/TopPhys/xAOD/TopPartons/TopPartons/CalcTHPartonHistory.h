/*
  Copyright (C) 2002-2017 CERN for the benefit of the ATLAS collaboration
*/

#ifndef ANALYSISTOP_TOPPARTONS_CALCTHPARTONHISTORY_H
#define ANALYSISTOP_TOPPARTONS_CALCTHPARTONHISTORY_H


// Framework include(s):
#include "TopPartons/CalcTopPartonHistory.h"
#include "xAODTruth/TruthParticleContainer.h"
#include "TopPartons/PartonHistory.h"

// forward declaration(s):
namespace top{
  class TopConfig;
}

namespace top{

  class CalcTHPartonHistory : public CalcTopPartonHistory{
    public:
      explicit CalcTHPartonHistory( const std::string& name );
      virtual ~CalcTHPartonHistory() {}

      //Struct for tH Data
      struct tH_values{
        TLorentzVector& H_p4;
        TLorentzVector& Tau1_from_Higgs_p4;
        int& Tau1_from_Higgs_pdgId;
        TLorentzVector& Tau2_from_Higgs_p4;
        int& Tau2__from_Higgs_pdgId;
        TLorentzVector& nu_from_Tau1_p4;
        int& nu_from_Tau1_pdgId;
        TLorentzVector& nu_from_Tau2_p4;
        int& nu_from_Tau2_pdgId;
        TLorentzVector& W_from_tau1_p4;
        int& W_from_tau1_pdgId;
        TLorentzVector W_from_tau2_p4;
        int& W_from_tau2_pdgId;
        TLorentzVector& W_hadr_decay1_from_tau1_p4;
        int& W_hadr_decay1_from_tau1_pdgId;
        TLorentzVector& W_hadr_decay2_from_tau1_p4;
        int& W_hadr_decay2_from_tau1_pdgId;
        TLorentzVector& W_lept_decay1_from_tau2_p4;
        int& W_lept_decay1_from_tau2_pdgId;
        TLorentzVector& W_lept_decay2_from_tau2_p4;
        int& W_lept_decay1_from_tau2_pdgId;
      }tH;
      //Storing parton history for ttbar resonance analysis
      CalcTHPartonHistory(const CalcTHPartonHistory& rhs) = delete;
      CalcTHPartonHistory(CalcTHPartonHistory&& rhs) = delete;
      CalcTHPartonHistory& operator=(const CalcTHPartonHistory& rhs) = delete;
      
	  void THHistorySaver(const xAOD::TruthParticleContainer * truthParticles, xAOD::PartonHistory * THPartonHistory);
	  
	  ///Store the four-momentum of H
      //bool Higgs(const xAOD::TruthParticleContainer* truthParticles, int start, TLorentzvector& Higgs);

      ///Store the four-momentum of several particles in the Higgs decay chain
      bool Higgstautau( const xAOD::TruthParticleContainer* truthParticles, int start);

	
	  virtual StatusCode execute();

  };
  

}

#endif
