/*
  Copyright (C) 2002-2017 CERN for the benefit of the ATLAS collaboration
*/

#include "TopPartons/CalcTHPartonHistory.h"
#include "TopPartons/CalcTtbarPartonHistory.h"
#include "TopPartons/CalcTopPartonHistory.h"
#include "TopConfiguration/TopConfig.h"


namespace top {

	CalcTHPartonHistory::CalcTHPartonHistory(const std::string& name) : CalcTopPartonHistory(name) {}
	bool CalcTHPartonHistory::Higgstautau(const xAOD::TruthParticleContainer* truthParticles, int start) {

		bool has_Higgs = false;
		bool has_hadronic_decays = false;
		bool has_leptonic_decays = false;
		bool has_tau1_neutrino = false;
		bool has_tau2_neutrino = false;
		for (const xAOD::TruthParticle* particle : *truthParticles) {
			if (particle->pdgId() != start)	continue;

			tH.Higgs_p4 = particle->p4(); // top before FSR
			has_Higgs = true;
			for (size_t k = 0; k < particle->nChildren(); k++) {
				const xAOD::TruthParticle* HiggsChildren = particle->child(k);
				if (fabs(HiggsChildren->pdgId()) == 17) {// demanding tau-leptons as childen
					for (size_t q = 0; q < HiggsChildren->nChildren(); ++q) {
						if (fabs(tauChildren->pdgId()) == 24) { // demanding W-bosons as childen of tau-leptons
							for (size_t p = 0; p < tauChildren->nChildren(); ++p) {
								const xAOD::TruthParticle* WChildren = tauChildren->child(p); //check if hadronic or leptonic
								if (fabs(WChildren->pdgID()) >= 1 && fabs(WChildren->pdgID()) <= 5) { //hadronic decay
									if (p == 1) {
										tH.W_hadr_decay1_from_tau1_p4 = WChildren->p4();
										tH.W_hadr_decay1_from_tau1_pdgId = WChildren->pdgID();
									}
									else if (p == 2) {
										tH.Tau1_from_Higgs_p4 = HiggsChildren->p4();
										tH.Tau1_from_Higgs_pdgId = HiggsChildren->pdgID();

										tH.W_from_tau1_p4 = tauChildren->p4();
										tH.W_from_tau1_pdgId = tauChildren->pdgID();

										tH.W_hadr_decay1_from_tau1_p4 = WChildren->p4();
										tH.W_hadr_decay1_from_tau1_pdgId = WChildren->pdgID();

										tH.W_hadr_decay2_from_tau1_p4 = WChildren->p4();
										tH.W_hadr_decay2_from_tau1_pdgId = WChildren->pdgID();

										has_hadronic_decays = true;

										if (fabs(tauChildren->child((p + 1) % 2)->pdgID()) == 18) {
											tH.nu_from_Tau1_p4 = tauChildren->child((p + 1) % 2)->p4();
											tH.nu_from_Tau1_pdgId = tauChildren->child((p + 1) % 2)->pdgID();
											has_tau1_neutrino = true;
										}
									} // p==2
								}
								else if (fabs(WChildren->pdgID()) >= 11 && fabs(WChildren->pdgID()) <= 14) { //leptonic decay
									if (p == 1) {
										tH.W_lept_decay1_from_tau2_p4 = WChildren->p4();
										tH.W_lept_decay1_from_tau2_pdgId = WChildren->pdgID();
									}//p=1
									else if (p == 2) {
										tH.Tau1_from_Higgs_p4 = HiggsChildren->p4();
										tH.Tau1_from_Higgs_pdgId = HiggsChildren->pdgID();

										tH.W_from_tau1_p4 = tauChildren->p4();
										tH.W_from_tau1_pdgId = tauChildren->pdgID();

										tH.W_lept_decay1_from_tau2_p4 = WChildren->p4();
										tH.W_lept_decay1_from_tau2_pdgId = WChildren->pdgID();

										tH.W_lept_decay2_from_tau2_p4 = WChildren->p4();
										tH.W_lept_decay2_from_tau2_pdgId = WChildren->pdgID();

										if (fabs(tauChildren->child((p + 1) % 2)->pdgID()) == 18) {
											tH.nu_from_Tau2_p4 = tauChildren->child((p + 1) % 2)->p4();
											tH.nu_from_Tau2_pdgId = tauChildren->child((p + 1) % 2)->pdgID();
											has_tau2_neutrino = true;
										}
									}//p=2
								}//leptonic
							}
						}//if
					}//for
				} // if
			}//for
		} //for
		return false;
	}
	void CalcTHPartonHistory::THHistorySaver(const xAOD::TruthParticleContainer* truthParticles, xAOD::PartonHistory* THPartonHistory) {
		THPartonHistory->IniVarTH();

		TLorentzVector t_before, t_after, t_after_SC;
		TLorentzVector Wp;
		TLorentzVector b;
		TLorentzVector WpDecay1;
		TLorentzVector WpDecay2;
		int WpDecay1_pdgId;
		int WpDecay2_pdgId;

		bool event_top = CalcTopPartonHistory::topWb(truthParticles, 6, t_before, t_after, Wp, b, WpDecay1, WpDecay1_pdgId, WpDecay2, WpDecay2_pdgId);
		bool event_top_SC = CalcTopPartonHistory::topAfterFSR_SC(truthParticles, 6, t_after_SC);

		TLorentzVector tbar_before, tbar_after, tbar_after_SC;
		TLorentzVector Wm;
		TLorentzVector bbar;
		TLorentzVector WmDecay1;
		TLorentzVector WmDecay2;
		int WmDecay1_pdgId;
		int WmDecay2_pdgId;
		bool event_topbar = CalcTopPartonHistory::topWb(truthParticles, -6, tbar_before, tbar_after, Wm, bbar, WmDecay1, WmDecay1_pdgId, WmDecay2, WmDecay2_pdgId);
		bool event_topbar_SC = CalcTopPartonHistory::topAfterFSR_SC(truthParticles, -6, tbar_after_SC);

		if (event_top) {
			THPartonHistory->auxdecor< float >("MC_t_beforeFSR_m") = t_before.M();
			THPartonHistory->auxdecor< float >("MC_t_beforeFSR_pt") = t_before.Pt();
			THPartonHistory->auxdecor< float >("MC_t_beforeFSR_phi") = t_before.Phi();
			fillEtaBranch(THPartonHistory, "MC_t_beforeFSR_eta", t_before);

			THPartonHistory->auxdecor< float >("MC_t_afterFSR_m") = t_after.M();
			THPartonHistory->auxdecor< float >("MC_t_afterFSR_pt") = t_after.Pt();
			THPartonHistory->auxdecor< float >("MC_t_afterFSR_phi") = t_after.Phi();
			fillEtaBranch(THPartonHistory, "MC_t_afterFSR_eta", t_after);

			if (event_top_SC) {
				THPartonHistory->auxdecor< float >("MC_t_afterFSR_SC_m") = t_after_SC.M();
				THPartonHistory->auxdecor< float >("MC_t_afterFSR_SC_pt") = t_after_SC.Pt();
				THPartonHistory->auxdecor< float >("MC_t_afterFSR_SC_phi") = t_after_SC.Phi();
				fillEtaBranch(THPartonHistory, "MC_t_afterFSR_SC_eta", t_after_SC);
			}

			THPartonHistory->auxdecor< float >("MC_W_from_t_m") = Wp.M();
			THPartonHistory->auxdecor< float >("MC_W_from_t_pt") = Wp.Pt();
			THPartonHistory->auxdecor< float >("MC_W_from_t_phi") = Wp.Phi();
			fillEtaBranch(THPartonHistory, "MC_W_from_t_eta", Wp);

			THPartonHistory->auxdecor< float >("MC_b_from_t_m") = b.M();
			THPartonHistory->auxdecor< float >("MC_b_from_t_pt") = b.Pt();
			THPartonHistory->auxdecor< float >("MC_b_from_t_phi") = b.Phi();
			fillEtaBranch(THPartonHistory, "MC_b_from_t_eta", b);

			THPartonHistory->auxdecor< float >("MC_Wdecay1_from_t_m") = WpDecay1.M();
			THPartonHistory->auxdecor< float >("MC_Wdecay1_from_t_pt") = WpDecay1.Pt();
			THPartonHistory->auxdecor< float >("MC_Wdecay1_from_t_phi") = WpDecay1.Phi();
			THPartonHistory->auxdecor< int >("MC_Wdecay1_from_t_pdgId") = WpDecay1_pdgId;
			fillEtaBranch(THPartonHistory, "MC_Wdecay1_from_t_eta", WpDecay1);

			THPartonHistory->auxdecor< float >("MC_Wdecay2_from_t_m") = WpDecay2.M();
			THPartonHistory->auxdecor< float >("MC_Wdecay2_from_t_pt") = WpDecay2.Pt();
			THPartonHistory->auxdecor< float >("MC_Wdecay2_from_t_phi") = WpDecay2.Phi();
			THPartonHistory->auxdecor< int >("MC_Wdecay2_from_t_pdgId") = WpDecay2_pdgId;
			fillEtaBranch(THPartonHistory, "MC_Wdecay2_from_t_eta", WpDecay2);
		}//if
		if (event_topbar) {

			THPartonHistory->auxdecor< float >("MC_tbar_beforeFSR_m") = tbar_before.M();
			THPartonHistory->auxdecor< float >("MC_tbar_beforeFSR_pt") = tbar_before.Pt();
			THPartonHistory->auxdecor< float >("MC_tbar_beforeFSR_phi") = tbar_before.Phi();
			fillEtaBranch(THPartonHistory, "MC_tbar_beforeFSR_eta", tbar_before);

			THPartonHistory->auxdecor< float >("MC_tbar_afterFSR_m") = tbar_after.M();
			THPartonHistory->auxdecor< float >("MC_tbar_afterFSR_pt") = tbar_after.Pt();
			THPartonHistory->auxdecor< float >("MC_tbar_afterFSR_phi") = tbar_after.Phi();
			fillEtaBranch(THPartonHistory, "MC_tbar_afterFSR_eta", tbar_after);

			if (event_topbar_SC) {
				THPartonHistory->auxdecor< float >("MC_tbar_afterFSR_SC_m") = tbar_after_SC.M();
				THPartonHistory->auxdecor< float >("MC_tbar_afterFSR_SC_pt") = tbar_after_SC.Pt();
				THPartonHistory->auxdecor< float >("MC_tbar_afterFSR_SC_phi") = tbar_after_SC.Phi();
				fillEtaBranch(THPartonHistory, "MC_tbar_afterFSR_SC_eta", tbar_after_SC);
			}

			THPartonHistory->auxdecor< float >("MC_W_from_tbar_m") = Wm.M();
			THPartonHistory->auxdecor< float >("MC_W_from_tbar_pt") = Wm.Pt();
			THPartonHistory->auxdecor< float >("MC_W_from_tbar_phi") = Wm.Phi();
			fillEtaBranch(THPartonHistory, "MC_W_from_tbar_eta", Wm);

			THPartonHistory->auxdecor< float >("MC_b_from_tbar_m") = bbar.M();
			THPartonHistory->auxdecor< float >("MC_b_from_tbar_pt") = bbar.Pt();
			THPartonHistory->auxdecor< float >("MC_b_from_tbar_phi") = bbar.Phi();
			fillEtaBranch(THPartonHistory, "MC_b_from_tbar_eta", bbar);

			THPartonHistory->auxdecor< float >("MC_Wdecay1_from_tbar_m") = WmDecay1.M();
			THPartonHistory->auxdecor< float >("MC_Wdecay1_from_tbar_pt") = WmDecay1.Pt();
			THPartonHistory->auxdecor< float >("MC_Wdecay1_from_tbar_phi") = WmDecay1.Phi();
			THPartonHistory->auxdecor< int >("MC_Wdecay1_from_tbar_pdgId") = WmDecay1_pdgId;
			fillEtaBranch(THPartonHistory, "MC_Wdecay1_from_tbar_eta", WmDecay1);

			THPartonHistory->auxdecor< float >("MC_Wdecay2_from_tbar_m") = WmDecay2.M();
			THPartonHistory->auxdecor< float >("MC_Wdecay2_from_tbar_pt") = WmDecay2.Pt();
			THPartonHistory->auxdecor< float >("MC_Wdecay2_from_tbar_phi") = WmDecay2.Phi();
			THPartonHistory->auxdecor< int >("MC_Wdecay2_from_tbar_pdgId") = WmDecay2_pdgId;
			fillEtaBranch(THPartonHistory, "MC_Wdecay2_from_tbar_eta", WmDecay2);
		}//if
		//CalcTHPartonHistory::tH_values *tH;
		bool event_Higgs = CalcTHPartonHistory::Higgstautau(truthParticles, 25);

		if (event_Higgs) {
			//Higgs-Boson
			tHPartonHistory->auxdecor< float >("MC_Higgs_m") = tH.Higgs.M();
			tHPartonHistory->auxdecor< float >("MC_Higgs_pt") = tH.Higgs.Pt();
			tHPartonHistory->auxdecor< float >("MC_Higgs_phi") = tH.Higgs.Phi();
			fillEtaBranch(tHPartonHistory, "MC_Higgs_eta", tH.Higgs);

			//First Tau (Tau1)
			tHPartonHistory->auxdecor< float >("MC_tau1_from_Higgs_m") = tH.Tau1_from_Higgs_p4.M();
			tHPartonHistory->auxdecor< float >("MC_tau1_from_Higgs_pt") = tH.Tau1_from_Higgs_p4.Pt();
			tHPartonHistory->auxdecor< float >("MC_tau1_from_Higgs_phi") = tH.Tau1_from_Higgs_p4.Phi();
			tHPartonHistory->auxdecor< int >("MC_tau1_from_Higgs_phi") = tH.Taudecay1_pdgId;
			fillEtaBranch(tHPartonHistory, "MC_tau1_from_Higgs_eta", tH.Tau1_from_Higgs_p4);

			//Second Tau (Tau2)
			tHPartonHistory->auxdecor< float >("MC_tau2_from_Higgs_m") = tH.Tau2_from_Higgs_p4.M();
			tHPartonHistory->auxdecor< float >("MC_tau2_from_Higgs_pt") = tH.Tau2_from_Higgs_p4.Pt();
			tHPartonHistory->auxdecor< float >("MC_tau2_from_Higgs_phi") = tH.Tau2_from_Higgs_p4.Phi();
			tHPartonHistory->auxdecor< int >("MC_tau2_from_Higgs_pdgID") = tH.Taudecay2_pdgId;
			fillEtaBranch(tHPartonHistory, "MC_tau2_from_Higgs_eta", tH.Tau2_from_Higgs_p4);

			//neutrino from first Tau (Tau1)
			tHPartonHistory->auxdecor< float >("MC_nu_from_tau1_m") = tH.nu_from_Tau1_p4.M();
			tHPartonHistory->auxdecor< float >("MC_nu_from_tau1_pt") = tH.nu_from_Tau1_p4.Pt();
			tHPartonHistory->auxdecor< float >("MC_nu_from_tau1_phi") = tH.nu_from_Tau1_p4.Phi();
			tHPartonHistory->auxdecor< int >("MC_nu_from_tau1_pdgID") = tH.nu_from_Tau1_pdgId;
			fillEtaBranch(tHPartonHistory, "MC_nu_from_tau1_eta", tH.nu_from_Tau1_p4);

			//neutrino from second Tau (Tau2)
			tHPartonHistory->auxdecor< float >("MC_nu_from_tau2_m") = tH.nu_from_Tau2_p4.M();
			tHPartonHistory->auxdecor< float >("MC_nu_from_tau2_pt") = tH.nu_from_Tau2_p4.Pt();
			tHPartonHistory->auxdecor< float >("MC_nu_from_tau2_phi") = tH.nu_from_Tau2_p4.Phi();
			tHPartonHistory->auxdecor< int >("MC_nu_from_tau2_pdgID") = tH.nu_from_Tau2_pdgId;
			fillEtaBranch(tHPartonHistory, "MC_nu_from_tau2_eta", tH.nu_from_Tau2_p4);

			//W-Boson from first Tau (Tau1)
			tHPartonHistory->auxdecor< float >("MC_W_from_tau1_m") = tH.W_from_tau1_p4.M();
			tHPartonHistory->auxdecor< float >("MC_W_from_tau1_pt") = tH.W_from_Tau1_p4.Pt();
			tHPartonHistory->auxdecor< float >("MC_W_from_tau1_phi") = tH.W_from_Tau1_p4.Phi();
			tHPartonHistory->auxdecor< int >("MC_W_from_tau1_pdgID") = tH.W_from_Tau1_pdgId;
			fillEtaBranch(tHPartonHistory, "MC_W_from_tau1_eta", tH.W_from_Tau1_p4);

			//W-Boson from second Tau (Tau2)
			tHPartonHistory->auxdecor< float >("MC_W_from_tau2_m") = tH.W_from_tau2_p4.M();
			tHPartonHistory->auxdecor< float >("MC_W_from_tau2_pt") = tH.W_from_Tau2_p4.Pt();
			tHPartonHistory->auxdecor< float >("MC_W_from_tau2_phi") = tH.W_from_Tau2_p4.Phi();
			tHPartonHistory->auxdecor< int >("MC_W_from_tau2_pdgID") = tH.W_from_Tau2_pdgId;
			fillEtaBranch(tHPartonHistory, "MC_W_from_tau2_eta", tH.W_from_Tau2_p4);

			//First hadronic decay product from the W-Boson from Tau1
			tHPartonHistory->auxdecor< float >("W_hadr_decay1_from_tau1_m") = tH.W_hadr_decay1_from_tau1_p4.M();
			tHPartonHistory->auxdecor< float >("W_hadr_decay1_from_tau1_pt") = tH.W_hadr_decay1_from_tau1_p4.Pt();
			tHPartonHistory->auxdecor< float >("W_hadr_decay1_from_tau1_phi") = tH.W_hadr_decay1_from_tau1_p4.Phi();
			tHPartonHistory->auxdecor< int >("W_hadr_decay1_from_tau1_pdgID") = tH.W_hadr_decay1_from_tau1_pdgId;
			fillEtaBranch(tHPartonHistory, "W_hadr_decay1_from_tau1_eta", tH.W_hadr_decay1_from_tau1_p4);

			//Second hadronic decay product from the W-Boson from Tau1
			tHPartonHistory->auxdecor< float >("W_hadr_decay2_from_tau1_m") = tH.W_hadr_decay2_from_tau1_p4.M();
			tHPartonHistory->auxdecor< float >("W_hadr_decay2_from_tau1_pt") = tH.W_hadr_decay2_from_tau1_p4.Pt();
			tHPartonHistory->auxdecor< float >("W_hadr_decay2_from_tau1_phi") = tH.W_hadr_decay2_from_tau1_p4.Phi();
			tHPartonHistory->auxdecor< int >("W_hadr_decay2_from_tau1_pdgID") = tH.W_hadr_decay2_from_tau1_pdgId;
			fillEtaBranch(tHPartonHistory, "W_hadr_decay2_from_tau1_eta", tH.W_hadr_decay2_from_tau1_p4);

			//First leptonic decay product from the W-Boson from Tau2
			tHPartonHistory->auxdecor< float >("W_lept_decay1_from_tau2_m") = tH.W_lept_decay1_from_tau2_p4.M();
			tHPartonHistory->auxdecor< float >("W_lept_decay1_from_tau2_pt") = tH.W_lept_decay1_from_tau2_p4.Pt();
			tHPartonHistory->auxdecor< float >("W_lept_decay1_from_tau2_phi") = tH.W_lept_decay1_from_tau2_p4.Phi();
			tHPartonHistory->auxdecor< int >("W_lept_decay1_from_tau2_pdgID") = tH.W_lept_decay1_from_tau2_pdgId;
			fillEtaBranch(tHPartonHistory, "W_lept_decay1_from_tau2_eta", tH.W_lept_decay1_from_tau2_p4);

			//Second leptonic decay product from the W-Boson from Tau2
			tHPartonHistory->auxdecor< float >("W_lept_decay2_from_tau2_m") = tH.W_lept_decay2_from_tau2_p4.M();
			tHPartonHistory->auxdecor< float >("W_lept_decay2_from_tau2_pt") = tH.W_lept_decay2_from_tau2_p4.Pt();
			tHPartonHistory->auxdecor< float >("W_lept_decay2_from_tau2_phi") = tH.W_lept_decay2_from_tau2_p4.Phi();
			tHPartonHistory->auxdecor< int >("W_lept_decay2_from_tau2_pdgID") = tH.W_lept_decay2_from_tau2_pdgId;
			fillEtaBranch(tHPartonHistory, "W_lept_decay2_from_tau2_eta", tH.W_lept_decay2_from_tau2_p4);
		}//if

	}
	
}
  StatusCode CalcTHPartonHistory::execute()
  {
	 //CalcTHPartonhistory *tHml = new CalcTHPartonhistory();
     Get the Truth Particles
     const xAOD::TruthParticleContainer* truthParticles(nullptr);
     ATH_CHECK( evtStore()->retrieve( truthParticles , m_config->sgKeyMCParticle() ) );

     // Create the partonHistory xAOD object
     xAOD::PartonHistoryAuxContainer* partonAuxCont = new xAOD::PartonHistoryAuxContainer{};
     xAOD::PartonHistoryContainer* partonCont = new xAOD::PartonHistoryContainer{};
     partonCont->setStore( partonAuxCont );

     xAOD::PartonHistory* THPartonHistory = new xAOD::PartonHistory{};
     partonCont->push_back( THPartonHistory );

     // Recover the parton history for TH events
     tHml->THHistorySaver(truthParticles, THPartonHistory);

     // Save to StoreGate / TStore
     std::string outputSGKey = m_config->sgKeyTopPartonHistory();
     std::string outputSGKeyAux = outputSGKey + "Aux.";

     xAOD::TReturnCode save = evtStore()->tds()->record( partonCont , outputSGKey );
     xAOD::TReturnCode saveAux = evtStore()->tds()->record( partonAuxCont , outputSGKeyAux );
     if( !save || !saveAux ){
       return StatusCode::FAILURE;
     }

     return StatusCode::SUCCESS;
  }
}
